const	incomingWebhook = 	"https://hooks.slack.com/services/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" || process.env.hook,
	requestify =	require('requestify');

exports.sendMessage = function(message){
	requestify.post(incomingWebhook, message);
};