const	express = 	require('express'),
	app =		express(),
	bodyParser = 	require('body-parser'),
	port = 		process.env.PORT || 8080,
	slack = 	require('./slack.js');

app.use(bodyParser.json());

app.post('/', function(req, res){
	// console.log("req is: ", req);
	// console.log("####################################");
	// console.log("res is: ", res);

	//console.log("req is: ", req.body.issue.fields);

	if (req.body.webhookEvent == 'jira:issue_created'){

		if(req.body.issue.fields.customfield_10404 != null && req.body.issue.fields.customfield_10404 != ''){
			var str = customfield_10404.toLowerCase();
			var patt = new RegExp("/Lumia/g");
			var result = patt.test(str);
			
			if(result){
				slack.sendMessage({
					"text" : "Brace yourself, it's a Nokia ticket!",
						"attachments" : [{
							"title": "Oh no...",
							"text": "'" + req.body.issue.fields.customfield_10402 + ""
						}]
				});
			}

		}


		// slack.sendMessage({
		// 	"text": "an issue was created",
		// 	    "attachments": [
		// 	        {
		// 	            "title": "'" + req.body.issue.key +  "'",
		// 	            "title_link":"https://jira.telescope.tv/browse/" + "'" + req.body.issue.key + "'",
		// 	            "fallback": "Hey, an issue was created",
		// 	            "callback_id": "issue"  + "'" + req.body.issue.key + "'",
		// 	            "color": "#3AA3E3",
		// 	            "attachment_type": "default",
		// 	            "actions": [
		// 	                {
		// 	                    "name": "ticket",
		// 	                    "text": "Check devices",
		// 	                    "type": "button",
		// 	                    "value": "devices"
		// 	                },
		// 	                {
		// 	                    "name": "ticket",
		// 	                    "text": "Assign to Alvin",
		// 	                    "type": "button",
		// 	                    "value": "assign"
		// 	                },
		// 	                {
		// 	                    "name": "ticket",
		// 	                    "text": "delete ticket",
		// 	                    "style": "good",
		// 	                    "type": "button",
		// 	                    "value": "delete",
		// 	                    "confirm": {
		// 	                        "title": "Are you sure?",
		// 	                        "text": "Dont you hate Nokia Lumia?",
		// 	                        "ok_text": "So much",
		// 	                        "dismiss_text": "Nah"
		// 	                    }
		// 	                }
		// 	            ]
		// 	        }
		// 	    ]
		// });
	}
	if (req.get('X-GitHub-Event') == 'fork'){
		slack.sendMessage({
			'text': '<' + req.body.sender.url + '|' + req.body.sender.login + '> forked <' + req.body.repository.url + '|' + req.body.repository.name + '>',
		});
	}
	res.sendStatus(200);
});

app.get('/', function(req, res){
	// slack.sendMessage({
	// 		'text': 'hi there!'
	// });
});

app.listen(port, function() {
    console.log('running on http://localhost:' + port);
});